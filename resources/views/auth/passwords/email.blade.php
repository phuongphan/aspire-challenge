@extends('layouts.auth')

@section('auth')
<div class="col-md-6">
  <div class="card mx-4">
    <div class="card-body p-4">
      <h1>{{ __('Reset Password') }}</h1>
      @if (session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
      @endif

      <form method="POST" action="{{ route('password.email') }}" class="mt-3 needs-validation" novalidate>
        @csrf
        
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">@</span>
          </div>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          <div class="invalid-feedback">
            {{ __('Please enter your email.') }}
          </div>
          @error('email')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <button class="btn btn-block btn-success" type="submit">{{ __('Send Password Reset Link') }}</button>
      </form>
    </div>
  </div>
</div>
@endsection
