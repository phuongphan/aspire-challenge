@extends('layouts.auth')

@section('auth')
<div class="col-md-6">
  <div class="card mx-4">
    <div class="card-body p-4">
      <h1>{{ __('Reset Password') }}</h1>

      <form method="POST" action="{{ route('password.update') }}" class="mt-3 needs-validation" novalidate>
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">@</span>
          </div>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('Your Email') }}">
          <div class="invalid-feedback">
            {{ __('Please enter your email.') }}
          </div>
          @error('email')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="icon-lock"></i>
            </span>
          </div>
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('New Password') }}">
          <div class="invalid-feedback">
            {{ __('Please enter your password.') }}
          </div>
          @error('password')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="input-group mb-4">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="icon-lock"></i>
            </span>
          </div>
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Repeat Your Password') }}">
          <div class="invalid-feedback">
            {{ __('Please enter your confirm password.') }}
          </div>
        </div>
        <button class="btn btn-block btn-success" type="submit">{{ __('Reset Password') }}</button>
      </form>
    </div>
  </div>
</div>
@endsection
