@extends('layouts.auth')

@section('auth')
<div class="col-md-8">
  <div class="card-group">
    <div class="card p-4">
      <div class="card-body">
        <h1>{{ __('Login') }}</h1>
        <p class="text-muted">{{ __('Sign In to your account') }}</p>
        <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate>
          @csrf
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="icon-user"></i>
              </span>
            </div>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('Your Email') }}">
            <div class="invalid-feedback">
              {{ __('Please enter your email.') }}
            </div>
            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="input-group mb-4">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="icon-lock"></i>
              </span>
            </div>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Your Password') }}">
            <div class="invalid-feedback">
              {{ __('Please enter your password.') }}
            </div>
            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group mb-4">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label class="form-check-label" for="remember">
                {{ __('Remember Me') }}
              </label>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <button type="submit" class="btn btn-primary px-4">
                {{ __('Login') }}
              </button>
            </div>
            @if (Route::has('password.request'))
            <div class="col-6 text-right">
              <a class="btn btn-link px-0" href="{{ route('password.request') }}">
                {{ __('Forgot Password?') }}
              </a>
            </div>
            @endif
          </div>
        </form>
      </div>
    </div>
    <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
      <div class="card-body text-center">
        <div>
          <h2>Sign up</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          @if (Route::has('register'))
          <a class="btn btn-primary active mt-3" href="{{ route('register') }}">{{ __('Register') }}</a>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
