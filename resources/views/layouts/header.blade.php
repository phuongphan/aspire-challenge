<header class="p-0 app-header navbar">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>

  <a class="navbar-brand" href="{{ url('/') }}">
    <img class="navbar-brand-full" src="{{ asset('images/logo.png') }}" width="89" height="25" alt="{{ config('app.name', 'Laravel') }}">
    <img class="navbar-brand-minimized" src="{{ asset('images/logo.png') }}" width="30" height="30" alt="{{ config('app.name', 'Laravel') }}">
  </a>

  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>

  <ul class="nav navbar-nav ml-auto pr-3">
    <li class="nav-item dropdown d-md-down-none">
      <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="icon-bell" style="position:relative; top: 4px;"></i>
        <span class="badge badge-pill badge-danger">5</span>
      </a>
      <span class="dropdown-menu dropdown-menu-right dropdown-menu-lg pt-0">
        <span class="dropdown-header text-center">
          <strong>You have 5 notifications</strong>
        </span>
        <a class="dropdown-item" href="#"><i class="icon-user-follow text-success"></i> New user registered</a>
        <a class="dropdown-item" href="#"><i class="icon-user-unfollow text-danger"></i> User deleted</a>
        <a class="dropdown-item" href="#"><i class="icon-chart text-info"></i> Sales report is ready</a>
        <a class="dropdown-item" href="#"><i class="icon-basket-loaded text-primary"></i> New client</a>
        <a class="dropdown-item" href="#"><i class="icon-speedometer text-warning"></i> Server overloaded</a>
        <span class="dropdown-header text-center"><strong>Server</strong></span>
        <a class="dropdown-item" href="#">
          <span class="text-uppercase mb-1">
            <small>
              <b>CPU Usage</b>
            </small>
          </span>
          <span class="progress progress-xs">
            <span class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></span>
          </span>
          <small class="text-muted">348 Processes. 1/4 Cores.</small>
        </a>
        <a class="dropdown-item" href="#">
          <span class="text-uppercase mb-1">
            <small>
              <b>Memory Usage</b>
            </small>
          </span>
          <span class="progress progress-xs">
            <span class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></span>
          </span>
          <small class="text-muted">11444GB/16384MB</small>
        </a>
        <a class="dropdown-item" href="#">
          <span class="text-uppercase mb-1">
            <small>
              <b>SSD 1 Usage</b>
            </small>
          </span>
          <span class="progress progress-xs">
            <span class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></span>
          </span>
          <small class="text-muted">243GB/256GB</small>
        </a>
      </span>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img class="img-avatar mx-1" src="{{ asset('images/avatars/6.jpg') }}" alt="{{ config('app.name', 'Laravel') }}">
      </a>
      <div class="dropdown-menu dropdown-menu-right shadow pt-0 mt-2">
        <span class="dropdown-header dropdown-header-user border-bottom-0 p-0">
          <span class="d-flex">
            <span class="">
              <img class="img-avatar-detail" src="{{ asset('images/avatars/6.jpg') }}" alt="{{ config('app.name', 'Laravel') }}">
            </span>
            <span class="px-3 py-1 flex-grow-1">
              <strong>{{ Auth::user()->name }}</strong><br>
              <small class="text-muted">{{ Auth::user()->email }}</small>
            </span>
          </span>
        </span>
        <div class="divider"></div>
        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> {{ __('admin.account-logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </li>
  </ul>
</header>