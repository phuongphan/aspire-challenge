<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link  rel="icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}" />
  <link  rel="shortcut icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
  @php

  $urls = [
    'logout' => route('logout'),
    'login' => route('login'),
    'loans' => route('loans.index'),
    'make_a_loan' => route('loans.store'),
    'loan_packages' => route('loan-packages.index'),
    'loan_repayments' => route('loan-repayments.index'),
    'repay' => route('loan-repayments.update', 'id'),
  ];
  @endphp
  <app
    id="app"
    site_name="{{ config('app.name', 'Laravel') }}"
    :urls="{{ json_encode($urls) }}"
    :user="{{ json_encode(Auth::user()->toArray()) }}">
    <template v-slot:logout-csrf>
      @csrf
    </template>
  </app>
</body>
</html>
