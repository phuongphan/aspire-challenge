import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: require('./pages/Home').default
    },
    {
      path: '/loan-packages',
      name: 'loan-packages',
      component: require('./pages/LoanPackage').default
    },
    {
      path: '/my-loans/:page?',
      name: 'my-loans',
      component: require('./pages/MyLoan').default
    },
    {
      path: '/my-loan/:id',
      name: 'my-loan-repayment',
      component: require('./pages/MyLoanRepayment').default
    },
  ]
})