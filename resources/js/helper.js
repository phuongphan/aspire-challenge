export const format_number = (number, decimals, dec_point, thousands_sep) => {
  // Strip all characters but numerical ones.
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec)
      return '' + Math.round(n * k) / k
    }
  // Fix for IE parseFloat(0.55).toFixed(0) = 0
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
  if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
      s[1] = s[1] || ''
      s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}

export const pagination = (total_items, current_page = 1, page_size = 10, max_pages = 10) => {
  let total_pages = Math.ceil(total_items / page_size)

  if (current_page < 1) {
    current_page = 1
  } 
  else if (current_page > total_pages) {
    current_page = total_pages
  }

  let start_page, end_page
  if (total_pages <= max_pages) {
    start_page = 1
    end_page = total_pages
  }
  else {
      let max_pages_before_current_page = Math.floor(max_pages / 2)
      let max_pages_after_current_page = Math.ceil(max_pages / 2) - 1

      if (current_page <= max_pages_before_current_page) {
        start_page = 1
        end_page = max_pages
      } 
      else if (current_page + max_pages_after_current_page >= total_pages) {
        start_page = total_pages - max_pages + 1
        end_page = total_pages
      }
      else {
        start_page = current_page - max_pages_before_current_page
        end_page = current_page + max_pages_after_current_page
      }
  }

  let start_index = (current_page - 1) * page_size
  let end_index = Math.min(start_index + page_size - 1, total_items - 1)

  let pages = Array.from(Array((end_page + 1) - start_page).keys()).map(i => start_page + i)

  return {
    total_items: total_items,
    current_page: current_page,
    page_size: page_size,
    total_pages: total_pages,
    start_page: start_page,
    end_page: end_page,
    start_index: start_index,
    end_index: end_index,
    pages: pages
  }
}