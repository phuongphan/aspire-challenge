import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null
  },
  mutations: {
    update(state, user) {
      state.user = user
    },
    reduce(state, amount) {
      state.user.amount -= amount
    },
  },
  getters: {
    user: state => state.user
  }
})