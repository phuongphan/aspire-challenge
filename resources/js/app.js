/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('jquery');
require('popper.js');
require('./bootstrap');
require('pace');
require('perfect-scrollbar');
require('@coreui/coreui');
require('chart.js');
require('../../node_modules/@coreui/coreui/dist/js/coreui.min.js');

window.Vue = require('vue');
import Vuex from 'vuex';
import { store } from './store/index';
import VueRouter from 'vue-router';
import App from './App.vue';
import routes from './routes';
import VueI18n from 'vue-i18n';
import messages from './lang';

import axios from 'axios';
Vue.prototype.$http = axios;

import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use([Vuex, VueRouter, VueI18n]);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en', // set locale
  fallbackLocale: 'en',
  messages
})

const app = new Vue({
    el: '#app',
    store,
    router: routes,
    i18n,
    components: {App}
});

/**
 * 
 */
(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();