<?php

use Illuminate\Database\Seeder;

class LoanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Loan::create([
            'description' => 'Car loan',
            'status' => \App\Loan::STATUS_INIT,
            'amount' => 10000000,
            'loan_package_id' => 2,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \App\Loan::create([
            'description' => 'Motobike loan',
            'status' => \App\Loan::STATUS_APPROVED,
            'amount' => 5000000,
            'loan_package_id' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \App\Loan::create([
            'description' => 'Home loan',
            'status' => \App\Loan::STATUS_CANCELED,
            'amount' => 500000000,
            'loan_package_id' => 2,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
