<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Phuong Phan',
            'email' => 'phuongphan.info@gmail.com',
            'password' => bcrypt('123456'),
            'amount' => '100000000000',
            'api_token' => str_random(60),
        ]);
    }
}
