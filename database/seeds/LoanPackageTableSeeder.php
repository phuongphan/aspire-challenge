<?php

use Illuminate\Database\Seeder;

class LoanPackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\LoanPackage::create([
            'name' => '4 weeks return, 5% interest rate',
            'term' => 4,
            'interest_rate' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        \App\LoanPackage::create([
            'name' => '8 weeks return, 8% interest rate',
            'term' => 8,
            'interest_rate' => 8,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
