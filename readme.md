# Aspire Challenge

## How to install
    // clone source code
    git clone https://gitlab.com/phuongphan/aspire-challenge.git aspire

    cd aspire/

    // copy .env file
    cp .env.example .env

    // config database

    // install app's dependencies
    composer install

    // install app's dependencies
    yarn install

    // build production
    yarn prod

    // init database
    php artisan migrate:refresh --seed

    // serving http://127.0.0.1:8080
    php artisan serve --port=8080

    // user created already
    user: phuongphan.info@gmail.com
    pass: 123456

## User story format
- The borrower accesses the loan application by registering an account or accessing an existing account.
- Loan application has many loan packages with different conditions, interest rates, paid weekly.
- Users need to enter the loan description, loan package and loan amount. The required loan amount must be under 1,000,000,000 vnd.
- After submitting the loan request, the status of the request will be initialized, canceled if the request is declined and approved status.
- If the request is approved, the borrower can access the details of the payables for each week until the end.
- The borrower will click the "repay" button, for many unpaid weeks, a warning message will occur below the "repay" button.
