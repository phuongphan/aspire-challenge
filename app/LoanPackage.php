<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Filterable;

class LoanPackage extends Model
{
    use Filterable;

    const STATUS_INIT = 0;
    const STATUS_PAID = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'term', 'interest_rate',
    ];

    /**
     * Filters
     */
    protected $filterable = [
        'id',
    ];

    public function filterKeyword($query, $value)
    {
        return $query->keyword($value);
    }

    /**
     * Scopes
     */
    public function scopeKeyword($query, $value)
    {
        return $query->where('name', 'like', '%' . $value . '%');
    }
}
