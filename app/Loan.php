<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Traits\Helpable;
use App\Traits\Filterable;

class Loan extends Model
{
    use Helpable, Filterable;

    const STATUS_INIT = 0;
    const STATUS_APPROVED = 1;
    const STATUS_CANCELED = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'status', 'amount', 'loan_package_id', 'created_by',
    ];

    public function loan_package()
    {
        return $this->belongsTo(LoanPackage::class);
    }

    public function repayments()
    {
        return $this->hasMany(LoanRepayment::class);
    }

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->created_by = Auth::check() ? Auth::user()->id : 1;
        });

        self::saved(function($model){
            if($model->status == self::STATUS_APPROVED && $model->repayments->count() == 0) {
                self::calculateWeekly($model);
            }
        });
    }

    protected static function calculateWeekly($model)
    {
        $principal = ($model->amount / $model->loan_package->term);
        collect(range(1, $model->loan_package->term))->each(function($term) use ($model, $principal) {
            // principal amount remaining
            $current_principal_remaining = $principal * $term;

            // calculate interest
            $interest = $current_principal_remaining * $model->loan_package->interest_rate / 100;

            // store loan_repayments
            LoanRepayment::create([
                'amount' => $principal + $interest,
                'status' => LoanRepayment::STATUS_INIT,
                'term' => date('Y-m-d', strtotime("+{$term} weeks")),
                'loan_id' => $model->id,
            ]);
        });
    }

    public function scopeMine($query)
    {
        return $query->where('created_by', Auth::user()->id);
    }

    /**
     * Filters
     */
    protected $filterable = [
        'id',
        'status',
        'created_by',
    ];

    public function filterKeyword($query, $value)
    {
        return $query->keyword($value);
    }

    /**
     * Scopes
     */
    public function scopeKeyword($query, $value)
    {
        return $query->where('description', 'like', '%' . $value . '%');
    }
}
