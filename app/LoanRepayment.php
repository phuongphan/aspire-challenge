<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Traits\Helpable;
use App\Traits\Filterable;

class LoanRepayment extends Model
{
    use Helpable, Filterable;
    
    const STATUS_INIT = 0;
    const STATUS_PAID= 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'status', 'term', 'loan_id', 'created_by',
    ];

    public function package()
    {
        return $this->hasOne(Loan::class);
    }

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->created_by = Auth::check() ? Auth::user()->id : 1;
        });
    }

    /**
     * Filters
     */
    protected $filterable = [];

    public function filterLoanId($query, $value)
    {
        return $query->loanId($value);
    }

    /**
     * Scopes
     */
    public function scopeLoanId($query, $value)
    {
        return $query->where('loan_id', $value);
    }
}
