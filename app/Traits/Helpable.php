<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait Helpable
{
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'created_by');
    }
}