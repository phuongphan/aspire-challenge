<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Loan;

class LoanController extends Controller
{
    const LIMIT = 10;

    public function index(Request $request)
    {
        $params = array_merge([
                'page' => 1,
                'keyword' => null,
            ],
            $request->all()
        );

        $result = Loan::filter($params)->mine()->latest()->paginate(self::LIMIT);
        collect($result->items())->map(function($row) {
            $row->amount_vnd = number_format($row->amount, 0, ',', '.');
            $row->loan_package_name = $row->loan_package->name;
            return $row;
        });

        return $result->toJson();
    }

    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'amount' => 'required|numeric|min:1|max:1000000000',
            'loan_package_id' => 'required|numeric|exists:loan_packages,id',
        ]);

        DB::beginTransaction();
        try {
            $loan = Loan::create($request->only(['description', 'loan_package_id', 'amount']));
            DB::commit();
        } 
        catch (Throwable $error) {
            DB::rollback();
        }

        return self::index($request);
    }
}
