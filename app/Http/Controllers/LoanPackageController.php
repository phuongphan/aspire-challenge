<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LoanPackage;

class LoanPackageController extends Controller
{
    const LIMIT = 10;

    public function index(Request $request)
    {
        $params = array_merge([
                'id' => null,
                'keyword' => null,
            ],
            $request->all()
        );

        return LoanPackage::filter($params)->latest()->paginate(self::LIMIT)->toJson();
    }
}
