<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\LoanRepayment;

class LoanRepaymentController extends Controller
{
    public function index(Request $request)
    {
        $params = array_merge([
                'loan_id' => null,
            ],
            $request->all()
        );

        return LoanRepayment::filter($params)->latest()->get()->map(function($row) {
            $row->amount_vnd = number_format($row->amount, 0, ',', '.');
            return $row;
        });
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric|exists:loan_repayments,id',
        ]);

        DB::beginTransaction();
        try {
            $loan_repayment = LoanRepayment::findOrFail(
                $request->input('id')
            );
            $user = Auth::user();

            // check the user's amount enough
            if($loan_repayment->amount > $user->amount) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'amount' => ['Your amount is not enough for repay'],
                ]);
            }

            // reduce the amount in the user's wallet
            $user->amount -= $loan_repayment->amount;
            $user->save();

            // paid
            $loan_repayment->status = LoanRepayment::STATUS_PAID;
            $loan_repayment->save();
            
            DB::commit();
        } 
        catch (Throwable $error) {
            DB::rollback();
        }

        return self::index($request);
    }
}
